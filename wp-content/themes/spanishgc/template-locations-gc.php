<?php

/*

Template Name: Locations in GC

*/

get_header(); 
the_post(); ?>

<div class="template-locations-gc">
    <div class="container-fluid">
        <div class="container-locations-gc">
            <div class="cta-back-title-general-locations-gc">
                <div class="title-locations-gc">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_locations_gc'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-locations-gc">

                <div class="container-sedes">
                    <div class="row">

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_tomas_morales = get_field('titulo_sede_tomas_morales');
                                $imagen_sede_tomas_morales = get_field('imagen_sede_tomas_morales');
                                if ($titulo_sede_tomas_morales && $imagen_sede_tomas_morales){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_tomas_morales'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_tomas_morales'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_tomas_morales'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-info-book-now">
                                        <a href="<?php the_field('pagina_sede_tomas_morales'); ?>">More info</a>
                                        <a href="<?php the_field('pagina_book_now_sede_tomas_morales'); ?>">Book now</a>
                                    </div>
                                    <div class="mapa-sede">
                                        <iframe src="<?php the_field('url_mapa_sede_tomas_morales'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_mesa_lopez = get_field('titulo_sede_mesa_lopez');
                                $imagen_sede_mesa_lopez = get_field('imagen_sede_mesa_lopez');
                                if ($titulo_sede_mesa_lopez && $imagen_sede_mesa_lopez){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_mesa_lopez'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_mesa_lopez'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_mesa_lopez'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-info-book-now">
                                        <a href="<?php the_field('pagina_sede_mesa_lopez'); ?>">More info</a>
                                        <a href="<?php the_field('pagina_book_now_sede_mesa_lopez'); ?>" target="_blank">Book now</a>
                                    </div>
                                    <div class="mapa-sede">
                                        <iframe src="<?php the_field('url_mapa_sede_mesa_lopez'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                        <div class="col-12 col-lg-4">
                            <?php
                                $titulo_sede_siete_palmas = get_field('titulo_sede_siete_palmas');
                                $imagen_sede_siete_palmas = get_field('imagen_sede_siete_palmas');
                                if ($titulo_sede_siete_palmas && $imagen_sede_siete_palmas){
                            ?>
                            <div class="cont-sede">
                                <div class="sede">
                                    <div class="titulo-sede">
                                        <?php the_field('titulo_sede_siete_palmas'); ?>
                                    </div>
                                    <a href="<?php the_field('pagina_sede_siete_palmas'); ?>">
                                        <div class="imagen-bg-sede" style="background-image: url('<?php the_field('imagen_sede_siete_palmas'); ?>')">
                                            <div class="overlay"><i class="fas fa-chevron-right"></i></div>
                                        </div>
                                    </a>
                                    <div class="ctas-info-book-now">
                                        <a href="<?php the_field('pagina_sede_siete_palmas'); ?>">More info</a>
                                        <a href="<?php the_field('pagina_book_now_sede_siete_palmas'); ?>" target="_blank">Book now</a>
                                    </div>
                                    <div class="mapa-sede">
                                        <iframe src="<?php the_field('url_mapa_sede_siete_palmas'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div> <?php // .col ?>

                    </div> <?php // .row ?>
                </div> <?php // .container-sedes ?>


            </div> <?php // . container-general-info-locations-gc ?>
            
         </div> <?php // .container-locations-gc ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-locations-gc ?>




<?php get_footer(); ?>