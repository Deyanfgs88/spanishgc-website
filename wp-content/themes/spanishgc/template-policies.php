<?php

/*

Template Name: Policies

*/

get_header(); 
the_post(); ?>

<div class="template-policies">
    <div class="container-fluid">
        <div class="container-policies">
            <div class="cta-back-title-general-policies">
                <div class="title-policies">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_politicas'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-policies">

                <?php the_field('contenido_politicas') ?>

            </div> <?php // . container-general-info-policies ?>
            
         </div> <?php // .container-policies ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-policies ?>




<?php get_footer(); ?>