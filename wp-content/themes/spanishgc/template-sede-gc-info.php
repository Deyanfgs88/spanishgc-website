<?php

/*

Template Name: Sede en Gran Canaria

*/

get_header(); 
the_post(); ?>

<div class="template-sede-gc">
    <div class="container-fluid">
        <div class="container-sede-gc">
            <div class="cta-back-title-general-sede-gc">
                <div class="title-sede-gc">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_sede_gc'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-sede-gc">
                <div class="row">

                    <div class="col-12 col-lg-6">
                        <div class="container-imagen-info">
                            <?php
                                $imagen_sede_gc = get_field('imagen_sede_gc');
                                if ($imagen_sede_gc){
                            ?>
                            <div class="imagen">
                                <img src="<?php the_field('imagen_sede_gc'); ?>" alt="imagen sede">
                            </div>
                            <?php } ?>

                            <?php
                                $texto_info_sede_gc = get_field('texto_info_sede_gc');
                                if ($texto_info_sede_gc){
                            ?>
                            <div class="texto-info-sede">
                                <?php the_field('texto_info_sede_gc'); ?>
                            </div>
                            <?php } ?>

                            <?php
                                $lista_info_sede_gc = get_field('lista_info_sede_gc');
                                if ($lista_info_sede_gc){
                            ?>
                            <div class="lista-info-sede">
                                <?php
                                    echo '<ul>';
                                    foreach ($lista_info_sede_gc as $item_info) {
                                        echo '<li><i class="fas fa-chevron-right"></i>' . $item_info['item_info_sede_gc'] . '</li>';
                                    }
                                    echo '</ul>';
                                ?>
                            </div>
                            <?php } ?>

                        </div> <?php // .container-video-info ?>
                    </div> <?php // .col ?>

                    <div class="col-12 col-lg-6">
                        <div class="container-mapa-info">

                            <?php
                                $mapa_sede_gc = get_field('mapa_sede_gc');
                                if ($mapa_sede_gc){
                            ?>
                            <div class="mapa">
                                <iframe src="<?php the_field('mapa_sede_gc'); ?>" frameborder="0" style="border:0" allowfullscreen></iframe>
                            </div>
                            <?php } ?>

                            <div class="cont-localizacion-horario">
                                <?php
                                    $direccion_sede = get_field('direccion_sede_gc');
                                    if ($direccion_sede){
                                ?>
                                <div class="item-info">
                                    <i class="fas fa-map-marker-alt"></i><?php the_field('direccion_sede_gc'); ?>
                                </div>
                                <?php } ?>
                                
                                <?php
                                    $telefono_sede = get_field('telefono_sede_gc');
                                    $tel_sin_espacios = str_replace(' ', '', $telefono_sede); 
                                    if ($telefono_sede){
                                ?>
                                <div class="item-info">
                                    <i class="fas fa-phone"></i><a href="tel:+34<?php echo $tel_sin_espacios; ?>"><?php the_field('telefono_sede_gc'); ?></a>
                                </div>
                                <?php } ?>

                                <?php
                                    $titulo_horario_academia = get_field('titulo_horario_academia_idiomas_sede_gc');
                                    $texto_horario_academia = get_field('texto_horario_academia_idiomas_sede_gc');
                                    if ($titulo_horario_academia && $texto_horario_academia){
                                ?>
                                <div class="item-info">
                                    <i class="fas fa-clock"></i>
                                    <div class="info">
                                        <div class="titulo-horario">
                                            <?php the_field('titulo_horario_academia_idiomas_sede_gc'); ?>
                                        </div>
                                        <?php the_field('texto_horario_academia_idiomas_sede_gc'); ?>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php
                                    $titulo_horario_extra_academia = get_field('titulo_oficina_idiomas_extranjero_sede_gc');
                                    $texto_horario_extra_academia = get_field('texto_horario_oficina_idiomas_extranjero_sede_gc');
                                    if ($titulo_horario_extra_academia && $texto_horario_extra_academia){
                                ?>
                                <div class="item-info">
                                    <i class="fas fa-clock"></i>
                                    <div class="info">
                                        <div class="titulo-horario">
                                            <?php the_field('titulo_oficina_idiomas_extranjero_sede_gc'); ?>
                                        </div>
                                        <?php the_field('texto_horario_oficina_idiomas_extranjero_sede_gc'); ?>
                                    </div>
                                </div>
                                <?php } ?>

                            </div>
                            
                        </div> <?php // .container-mapa-info ?>

                    </div> <?php // .col ?>

                </div> <?php // .row ?>
            </div> <?php // . container-general-info-sede-gc ?>
            
         </div> <?php // .container-sede-gc ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-sede-gc ?>


<?php get_footer(); ?>