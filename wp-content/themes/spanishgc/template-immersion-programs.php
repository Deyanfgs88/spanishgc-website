<?php

/*

Template Name: Immersion Programs

*/

get_header(); 
the_post(); ?>

<div class="template-programs">
    <div class="container-fluid">
        <div class="container-programs">

            <div class="cta-back-title-general-programs">
                <div class="title-programs">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>
            </div>
            
            <?php
                $texto_select_course = get_field('texto_select_curso');
                $list_select_courses = get_field('lista_select_cursos');
            ?>
            <div class="container-select-item-program">
                <?php
                    if ($texto_select_course) {
                ?>
                <p><?php the_field('texto_select_curso'); ?></p>
                <?php } ?>
                <?php
                    if ($list_select_courses) {
                ?>
                <select class="select-course">
                    <?php
                        $i = 0;
                        foreach ($list_select_courses as $course) {
                            $i++;
                            echo '<option value="curso-' . $i .'">' . $course['texto_curso'] . '</option>';
                        }
                    ?>
                </select>
                <?php } ?>
            </div>
            
            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div id="curso-1" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_immersive_spanish_courses'); ?>" alt="imagen immersive spanish courses">
                            <a href="<?php the_field('pagina_more_info_immersive_spanish_courses'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_immersive_spanish_courses'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_immersive_spanish_courses'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_immersive_spanish_courses'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_immersive_spanish_courses'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-2" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_experience_canarian_life'); ?>" alt="imagen experience canarian life">
                            <a href="<?php the_field('pagina_more_info_experience_canarian_life'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_experience_canarian_life'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_experience_canarian_life'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_experience_canarian_life'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_experience_canarian_life'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-3" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_gap_year_program'); ?>" alt="imagen gap year program">
                            <a href="<?php the_field('pagina_more_info_gap_year_program'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_gap_year_program'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_gap_year_program'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_gap_year_program'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_gap_year_program'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div id="curso-4" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_student_group_travel'); ?>" alt="imagen student group travel">
                            <a href="<?php the_field('pagina_more_info_student_group_travel'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_student_group_travel'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_student_group_travel'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_student_group_travel'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_student_group_travel'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-5" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_hiking'); ?>" alt="imagen hiking">
                            <a href="<?php the_field('pagina_more_info_hiking'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_hiking'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_hiking'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_hiking'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_hiking'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-6" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_spanish_surf'); ?>" alt="imagen spanish surf">
                            <a href="<?php the_field('pagina_more_info_spanish_surf'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_spanish_surf'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_spanish_surf'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_spanish_surf'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_spanish_surf'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-7" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_canarian_cooking_culture'); ?>" alt="imagen canarian cooking and culture">
                            <a href="<?php the_field('pagina_more_info_canarian_cooking_culture'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_canarian_cooking_culture'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_canarian_cooking_culture'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_canarian_cooking_culture'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_canarian_cooking_culture'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>
                        
        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-programs ?>



<?php get_footer(); ?>