<?php

/*

Template Name: General Programs

*/

get_header(); 
the_post(); ?>

<div class="template-programs">
    <div class="container-fluid">
        <div class="container-programs">

            <div class="cta-back-title-general-programs">
                <div class="title-programs">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>
            </div>
            
            <?php
                $texto_select_course = get_field('texto_select_curso');
                $list_select_courses = get_field('lista_select_cursos');
            ?>
            <div class="container-select-item-program">
                <?php
                    if ($texto_select_course) {
                ?>
                <p><?php the_field('texto_select_curso'); ?></p>
                <?php } ?>
                <?php
                    if ($list_select_courses) {
                ?>
                <select class="select-course">
                    <?php
                        $i = 0;
                        foreach ($list_select_courses as $course) {
                            $i++;
                            echo '<option value="curso-' . $i .'">' . $course['texto_curso'] . '</option>';
                        }
                    ?>
                </select>
                <?php } ?>
            </div>
            
            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div id="curso-1" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_general'); ?>" alt="imagen general">
                            <a href="<?php the_field('pagina_more_info_general'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_general'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_general'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_general'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_general'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-2" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_general_plus'); ?>" alt="imagen general plus">
                            <a href="<?php the_field('pagina_more_info_general_plus'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_general_plus'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_general_plus'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_general_plus'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_general_plus'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-3" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_intensive'); ?>" alt="imagen intensive">
                            <a href="<?php the_field('pagina_more_info_intensive'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_intensive'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_intensive'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_intensive'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_intensive'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div id="curso-4" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_exam_preparation'); ?>" alt="imagen exam preparation">
                            <a href="<?php the_field('pagina_more_info_exam_preparation'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_exam_preparation'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_exam_preparation'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_exam_preparation'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_exam_preparation'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-5" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_semi_private_lessons'); ?>" alt="imagen semi private lessons">
                            <a href="<?php the_field('pagina_more_info_semi_private_lessons'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_semi_private_lessons'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_semi_private_lessons'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_semi_private_lessons'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_semi_private_lessons'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-6" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_private_lessons'); ?>" alt="imagen private lessons">
                            <a href="<?php the_field('pagina_more_info_private_lessons'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_private_lessons'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_private_lessons'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_private_lessons'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_private_lessons'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-7" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_spanish_courses_kids_teenagers'); ?>" alt="imagen spanish courses kids and teenagers">
                            <a href="<?php the_field('pagina_more_info_spanish_courses_kids_teenagers'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_spanish_courses_kids_teenagers'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_spanish_courses_kids_teenagers'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_spanish_courses_kids_teenagers'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_spanish_courses_kids_teenagers'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-8" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_spanish_business'); ?>" alt="imagen spanish for business">
                            <a href="<?php the_field('pagina_more_info_spanish_business'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_spanish_business'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_spanish_business'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_spanish_business'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_spanish_business'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                    <div id="curso-9" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_dele_exam_preparation'); ?>" alt="imagen DELE Exam Preparation">
                            <a href="<?php the_field('pagina_more_info_dele_exam_preparation'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_dele_exam_preparation'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_dele_exam_preparation'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_dele_exam_preparation'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_dele_exam_preparation'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
   
                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>
                        
        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-programs ?>



<?php get_footer(); ?>