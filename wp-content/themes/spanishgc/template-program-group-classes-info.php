<?php

/*

Template Name: Program Information Group Classes

*/

get_header(); 
the_post(); ?>

<div class="template-programs-info">
    <div class="container-fluid">
        <div class="container-program-info">
            <div class="cta-back-title-general-curso">
                <div class="title-program-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_info_programa'); ?></h2>
                </div>
            </div>

            <div class="container-general-class-info">

                <div class="container-clase-info">
                    
                    <div class="cont-clase-info">
                        <div class="clase-info">
                            <div class="row">

                                <div class="col-12 col-lg-6">
                                    <div class="info left">
                                        <img src="<?php the_field('imagen_info_programa'); ?>" alt="imagen info program">
                                        <div class="titulo-curso">
                                            <?php the_field('titulo_info_programa'); ?>
                                        </div>
                                        <div class="small-info">
                                            <?php the_field('texto_info_programa'); ?>
                                        </div>
                                        <div class="cta-reserva-ahora">
                                            <a href="<?php the_field('pagina_book_now_programa'); ?>">Book now</a>
                                        </div>
                                    </div> <? // .info ?>
                                </div> <?php // .col ?>

                                <div class="col-12 col-lg-6">
                                    <div class="info right">
                                        <div class="sistema-bonos">
                                            <div class="icon-bonos">
                                                <i class="fas fa-clock"></i>
                                            </div>
                                            <div class="info-bonos">
                                                <div class="title-bonos">
                                                    <?php the_field('titulo_sistema_bonos_programa'); ?>
                                                </div>
                                                <div class="desc-bonos">
                                                    <?php the_field('descripcion_sistema_bonos_programa'); ?>
                                                </div>
                                            </div>
                                        </div> <?php // .sistema-bonos ?>

                                        <div class="alumnos-distri-inicio-certi">

                                            <div class="item alumnos">
                                                <i class="fas fa-users"></i>
                                                <div class="title-item">
                                                    <?php the_field ('titulo_numero_de_alumnos_programa'); ?>
                                                </div>
                                                <div class="text-item">
                                                    <?php the_field('texto_numero_de_alumnos_programa'); ?>
                                                </div>
                                            </div>

                                            <div class="item distribucion">
                                                <i class="fas fa-stopwatch"></i>
                                                <div class="title-item">
                                                    <?php the_field('titulo_distribucion_programa'); ?>
                                                </div>
                                                <div class="text-item">
                                                    <?php the_field('texto_distribucion_programa'); ?>
                                                </div>
                                            </div>

                                            <div class="item inicio">
                                                <i class="far fa-calendar-alt"></i>
                                                <div class="title-item">
                                                    <?php the_field('titulo_inicio_programa'); ?>
                                                </div>
                                                <div class="text-item">
                                                    <?php the_field('texto_inicio_programa'); ?>
                                                </div>
                                            </div>

                                            <div class="item certificacion">
                                                <i class="fas fa-trophy"></i>
                                                <div class="title-item">
                                                    <?php the_field('titulo_certificacion_programa'); ?>
                                                </div>
                                                <div class="text-item">
                                                    <?php the_field('texto_certificacion_programa'); ?>
                                                </div>
                                            </div>

                                        </div> <?php // .alumnos-distri-inicio-certi ?>

                                        <div class="container-general-precios">
                                            
                                            <div class="container-precios">
                                                <div class="icon-euros">
                                                    <i class="fas fa-euro-sign"></i>
                                                </div>
                                                <div class="cont-info-prices">
                                                    <div class="prices-hours">
                                                        <?php 
                                                            $list_prices = get_field('lista_precios_programa');
                                                            if($list_prices){
                                                                echo '<ul>';
                                                                foreach($list_prices as $prices){
                                                                    echo '<li><i class="fas fa-chevron-right"></i>' . $prices['precio'] . '</li>';
                                                                }
                                                                echo '</ul>';
                                                            } 
                                                        ?>
                                                        <div class="text-matricula">
                                                            <?php the_field('texto_anotacion_matricula_programa'); ?>
                                                        </div>
                                                    </div> <?php // . prices-hours ?>
    
                                                    <div class="list-coste-libros">
                                                        <div class="titulo-coste-libros">
                                                            <?php the_field('titulo_coste_libros'); ?>
                                                        </div>
                                                        <?php
                                                            $list_prices_books = get_field('lista_precios_libros_programa');
                                                            if ($list_prices_books){
                                                                echo '<ul>';
                                                                foreach ($list_prices_books as $prices_books) {
                                                                    echo '<li><i class="fas fa-chevron-right"></i><span class="nivel">' . $prices_books['nivel_programa'] . ': </span>' . $prices_books['coste_libro'] . '</li>';
                                                                }
                                                                echo '</ul>';
                                                            }
                                                        ?>
                                                    </div> <?php // .list-coste-libros ?>
                                                    
                                                    <?php
                                                        $texto_igic = get_field('texto_igic_programa');
                                                        if ($texto_igic){
                                                    ?>
                                                    <div class="texto-igic">
                                                        <?php the_field('texto_igic_programa'); ?>
                                                    </div>
                                                    <?php } ?>
    
                                                </div> <?php // .cont-info-prices ?>
                                            
                                            </div> <?php // . container-precios ?>

                                        </div> <?php // .container-general-precios ?>


                                    </div> <?php // .info ?>
                                </div> <?php // .col ?>
                            </div> <?php // .row ?>
                            
                        </div> <?php // .clase-info ?>
                    </div> <?php // .cont-clase-info ?>
    
                </div> <?php // .container-clase-info ?>
            </div> <?php // .container-general-class-info ?>


        </div> <?php // .container-program-info ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-programs-info ?>



<?php get_footer(); ?>