<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package spanishgc
 */

get_header();
?>

<div class="template-error-404">
	<div class="container-fluid">
		<div class="container-error-404">

			<div class="container-general-info-error-404">

				<div class="page-title">
					<h1><span>Oops! </span><?php esc_html_e( 'That page can&rsquo;t be found.', 'spanishgc' ); ?></h1>
				</div>
				<div class="text-404">
					<p>404</p>
				</div>
				<div class="cont-search">
					<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'spanishgc' ); ?></p>
					<div class="search">
						<?php get_search_form(); ?>
					</div>
				</div>
				
			</div> <?php // .container-general-info-error-404 ?>

		</div> <?php // .container-error-404 ?>
	</div> <?php // .container-fluid ?>
</div> <?php // .template-error-404 ?>


<?php get_footer(); ?>
