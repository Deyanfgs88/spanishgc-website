<?php

/*

Template Name: Spanish Kids Programs

*/

get_header(); 
the_post(); ?>

<div class="template-programs">
    <div class="container-fluid">
        <div class="container-programs">

            <div class="cta-back-title-general-programs">
                <div class="title-programs">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('title_programs'); ?></h2>
                </div>
            </div>

            <?php
                $info_spanish_kids_programs = get_field('main_info_spanish_kids_programs');
                if ($info_spanish_kids_programs){
            ?>
            <div class="container-main-info">
                <?php the_field('main_info_spanish_kids_programs'); ?>
            </div>
            <?php } ?>
            
            <?php
                $texto_select_course = get_field('texto_select_curso');
                $list_select_courses = get_field('lista_select_cursos');
            ?>
            <div class="container-select-item-program">
                <?php
                    if ($texto_select_course) {
                ?>
                <p><?php the_field('texto_select_curso'); ?></p>
                <?php } ?>
                <?php
                    if ($list_select_courses) {
                ?>
                <select class="select-course">
                    <?php
                        $i = 0;
                        foreach ($list_select_courses as $course) {
                            $i++;
                            echo '<option value="curso-' . $i .'">' . $course['texto_curso'] . '</option>';
                        }
                    ?>
                </select>
                <?php } ?>
            </div>
            
            <div class="container-general-class">

                <div class="container-clases">
                    
                    <div id="curso-1" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_general_spanish_kids'); ?>" alt="imagen general spanish kids">
                            <a href="<?php the_field('pagina_more_info_general_spanish_kids'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_general_spanish_kids'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_general_spanish_kids'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_general_spanish_kids'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_general_spanish_kids'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-2" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_general_plus_spanish_kids'); ?>" alt="imagen general plus spanish kids">
                            <a href="<?php the_field('pagina_more_info_general_plus_spanish_kids'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_general_plus_spanish_kids'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_general_plus_spanish_kids'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_general_plus_spanish_kids'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_general_plus_spanish_kids'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
    
                    <div id="curso-3" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_semi_private_lessons_spanish_kids'); ?>" alt="imagen semi private lessons spanish kids">
                            <a href="<?php the_field('pagina_more_info_semi_private_lessons_spanish_kids'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_semi_private_lessons_spanish_kids'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_semi_private_lessons_spanish_kids'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_semi_private_lessons_spanish_kids'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_semi_private_lessons_spanish_kids'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>
                    
                    <div id="curso-4" class="cont-clase">
                        <div class="clase">
                            <img src="<?php the_field('imagen_private_lessons_spanish_kids'); ?>" alt="imagen private lessons spanish kids">
                            <a href="<?php the_field('pagina_more_info_private_lessons_spanish_kids'); ?>">
                                <div class="titulo">
                                    <?php the_field('titulo_private_lessons_spanish_kids'); ?>
                                </div>
                            </a>
                            <div class="small-info">
                                <?php the_field('texto_private_lessons_spanish_kids'); ?>
                            </div>
                            <div class="ctas-info-reserva">
                                <a href="<?php the_field('pagina_more_info_private_lessons_spanish_kids'); ?>">More info</a>
                                <a href="<?php the_field('pagina_book_now_private_lessons_spanish_kids'); ?>">Book now</a>
                            </div>
                        </div> <?php // .clase ?>
                    </div>

                </div> <?php // .container-clases ?>
            </div> <?php // .container-general-class ?>
                        
        </div> <?php // .container-curso ?>
  </div> <?php // .container-fluid ?>  
</div> <?php // .template-programs ?>



<?php get_footer(); ?>