<?php

/*

Template Name: Teaching Method

*/

get_header(); 
the_post(); ?>

<div class="template-teaching-method">
    <div class="container-fluid">
        <div class="container-teaching-method">
            <div class="cta-back-title-general-teaching-method">
                <div class="title-teaching-method">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>
            </div>

            <div class="container-imagen-header-bg-title" style="background-image: url('<?php the_field('imagen_banner_header_teaching_method'); ?>');">
                <div class="titulo">
                    <?php the_field('titulo_teaching_method'); ?>
                </div>
            </div> <?php // .container-imagen-header-bg-title ?>

            <div class="container-general-info-teaching-method">

                <div class="texto-principal">
                    <?php the_field ('texto_principal_teaching_method'); ?>
                </div>

                <div class="item-info">
                    <div class="titulo">
                        <?php the_field('titulo_perfect_language_skill'); ?>
                    </div>
                    <div class="texto">
                        <?php the_field('texto_perfect_language_skill'); ?>
                    </div>
                </div>

                <div class="item-info key-steps">
                    <div class="titulo">
                        <?php the_field('titulo_key_steps'); ?>
                    </div>
                    <?php
                        $lista_key_steps = get_field('lista_key_steps');
                        if ($lista_key_steps) {
                    ?>
                    <div class="steps">
                        <ul>
                            <?php
                                foreach ($lista_key_steps as $key_steps) {
                                    echo '<li>';
                                    echo '<img src="' . $key_steps['imagen_step'] . '" alt="key step" />';
                                    echo '<div>' . $key_steps['texto_step'] . '</div>';
                                    echo'</li>';
                                }
                            ?>
                        </ul>
                    </div>
                    <?php } ?>
                </div>

                <div class="item-info our-levels">
                    <div class="titulo">
                        <?php the_field('titulo_our_levels'); ?>
                    </div>
                    <div class="imagen">
                        <img src="<?php the_field('imagen_our_levels'); ?>" alt="our levels" />
                    </div>
                </div>

                <div class="item-info">
                    <div class="titulo">
                        <?php the_field('titulo_certificates'); ?>
                    </div>
                    <div class="texto">
                        <?php the_field('texto_certificates'); ?>
                    </div>
                </div>
                
            </div> <?php // . container-general-info-teaching-method ?>
            
         </div> <?php // .container-teaching-method ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-teaching-method ?>




<?php get_footer(); ?>