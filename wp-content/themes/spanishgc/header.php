<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package spanishgc
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js" defer='defer'></script>

	<?php
		/**
		 *  Custom Title
		 */
		$title = get_post_meta($post->ID, 'etiquetatitle_83938', true);
		if ($title){
			echo '<title>' . $title . '</title>';
		} else{
			echo '<title>';
			wp_title('');
			echo '</title>';
		}
	?>

	<?php wp_head(); ?>

	<script>
		
		/**
		*  VALIDATE ORION FORM
		*/

		function validar(formaFrom) {
			var elFo = formaFrom;
			var pass = true;
			var mensaje = 'Se han detectado los siguientes errores:nn';
			if(document.forms["forma"+elFo]["nombre"].value == '') {
				pass = false;
				document.forms["forma"+elFo]["nombre"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["nombre"].style.background="none";
			}
			if(document.forms["forma"+elFo]["apellidos"].value == '') {
				pass = false;
				document.forms["forma"+elFo]["apellidos"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["apellidos"].style.background="none";
			}
			if(document.forms["forma"+elFo]["comentarios"].value == '') {
				pass = false;
				document.forms["forma"+elFo]["comentarios"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["comentarios"].style.background="none";
			}
			/*var reg = /^([A-Za-z0-9_-.])+@([A-Za-z0-9_-.])+.([A-Za-z]{2,4})$/;*/
			correo = document.forms["forma"+elFo]["emilio"].value;
			if(correo == '') {
				pass = false;
				document.forms["forma"+elFo]["emilio"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["emilio"].style.background="none";
			}
			if(document.forms["forma"+elFo]["telefono"].value == '' ) {
				pass = false;
				document.forms["forma"+elFo]["telefono"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["telefono"].style.background="none";
			}
			
			if(document.forms["forma"+elFo]["donde"].value == '0') {
				pass = false;
				document.forms["forma"+elFo]["donde"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["donde"].style.background="none";
			}
			if(document.forms["forma"+elFo]["idioma"].value == '0') {
				pass = false;
				document.forms["forma"+elFo]["idioma"].style.background="#FFD9D9";
			} else {
				document.forms["forma"+elFo]["idioma"].style.background="none";
			}
			if(!document.forms["forma"+elFo]["politica"].checked) {
				pass = false;
				document.getElementById("alert"+elFo).style.display="block";
				
			} else {
				document.getElementById("alert"+elFo).style.display="none";
			}

			if (pass) {
				document.forms["forma"+elFo].action = "https://www.lcidiomasgc.com/gracias/";
				document.forms["forma"+elFo].submit();
			}
		return
		}
	</script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"name": "Idiomas GC Tomás Morales",
			"image": "https://spanishgc.com/wp-content/uploads/2018/12/logo-idiomasgc-lc.png",
			"@id": "",
			"url": "https://spanishgc.com",
			"telephone": "+34928909054",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "Paseo Tomás Morales, 50",
				"addressLocality": "Las Palmas de Gran Canaria",
				"postalCode": "35003",
				"addressCountry": "ES"
			}
		}
	</script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"name": "Idiomas GC Mesa y López",
			"image": "https://spanishgc.com/wp-content/uploads/2018/12/logo-idiomasgc-lc.png",
			"@id": "",
			"url": "https://spanishgc.com",
			"telephone": "+34928909054",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "Calle República Dominicana, 17",
				"addressLocality": "Las Palmas de Gran Canaria",
				"postalCode": "35010",
				"addressCountry": "ES"
			}
		}
	</script>

	<script type="application/ld+json">
		{
			"@context": "http://schema.org",
			"@type": "LocalBusiness",
			"name": "Idiomas GC 7 Palmas",
			"image": "https://spanishgc.com/wp-content/uploads/2018/12/logo-idiomasgc-lc.png",
			"@id": "",
			"url": "https://spanishgc.com",
			"telephone": "+34928909054",
			"address": {
				"@type": "PostalAddress",
				"streetAddress": "Avenida Pintor Felo Monzón, Calle Lomo San Lázaro, 27",
				"addressLocality": "Las Palmas de Gran Canaria",
				"postalCode": "35019",
				"addressCountry": "ES"
			}
		}
	</script>

</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

	<header>
		<div class="container-header">

			<div class="container-logo">
				<?php
					$custom_logo_id = get_theme_mod( 'custom_logo' );
					$logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
					?>
					<a href="<?php echo get_home_url(); ?>">
						<img src="<?php echo $logo[0]; ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>">
					</a>
			</div> <?php //.container-logo ?>

			<div class="container-sobre-igc-ubic-menu">

				<div class="header-sup">
					<?php
						$telefono = get_theme_mod('phone');
						$tel_sin_espacios = str_replace(' ', '', $telefono);
						$tel_sin_parentesis = str_replace('(', '', $tel_sin_espacios);
						$tel_sin_parentesis = str_replace(')', '', $tel_sin_parentesis);
						$link_locations_gc = get_theme_mod('locations_gc');
						$texto_locations_gc = get_theme_mod('texto_locations_gc');
						$link_book_now = get_theme_mod('book_now');
						$texto_book_now = get_theme_mod('texto_book_now');
					?>
					<?php if ($telefono){ ?>
					<a href="tel:<?php echo $tel_sin_parentesis; ?>"><i class="fas fa-phone"></i><?php echo $telefono; ?></a>
					<?php } ?>
					<?php if ($texto_locations_gc){ ?>
					<a href="<?php echo get_permalink($link_locations_gc); ?>"><?php echo $texto_locations_gc; ?></a>
					<?php } ?>
					<?php if ($texto_book_now){ ?>
					<a href="<?php echo get_permalink($link_book_now); ?>"><?php echo $texto_book_now; ?></a>
					<?php } ?>
				</div>

				<div class="phone-menu">

					<div class="phone d-md-none">
						<?php  
							$telefono = get_theme_mod('phone');
							$tel_sin_espacios = str_replace(' ', '', $telefono);
							$tel_sin_parentesis = str_replace('(', '', $tel_sin_espacios);
							$tel_sin_parentesis = str_replace(')', '', $tel_sin_parentesis); 
 
						?>
						<a href="tel:<?php echo $tel_sin_parentesis; ?>"><i class="fas fa-phone"></i><?php echo $telefono; ?></a>
					</div>

					<div class="menu-desktop">
						<?php wp_nav_menu( array( 'theme_location' => 'menu-principal') ); ?>
					</div> <?php // .menu-desktop ?>

					<div class="menu-mobile">

						<!-- Icon Burguer -->
						<div class="icon-burguer-menu">
						<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#mydropdown" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
							<span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
  						</button> 
						</div>
						<!-- .Icon Burguer -->
						
					</div> <?php //.menu-mobile ?>

				</div> <?php // .phone-menu ?>

				<div class="collapse navbar-collapse custom-menu-mobile" id="mydropdown">
					<?php wp_nav_menu( array( 'theme_location' => 'menu-movil') ); ?>
				</div>

			</div> <?php //. container-sobre-igc-ubic-menu ?>

		</div> <?php //.container-header ?>

		<?php
			$link_why_grancanaria = get_theme_mod('why_gran_canaria');
			$link_teaching_method = get_theme_mod('teaching_method');
			$link_blog = get_theme_mod('blog');
			$link_meet_our_team = get_theme_mod('meet_our_team');
			$link_test_your_spanish = get_theme_mod('test_your_spanish');
			$link_student_zone = get_theme_mod('student_zone');
			if (!is_front_page()){
		?>
		<div class="container-ctas-pages d-none d-md-block">
			<div class="cont-pages">
				<div class="cont-cta-page">
					<a href="<?php echo get_permalink($link_why_grancanaria); ?>">Why Gran Canaria?</a>
				</div>
				<div class="cont-cta-page">
					<a href="<?php echo get_permalink($link_teaching_method); ?>">Teaching method</a>
				</div>
				<div class="cont-cta-page">
					<a href="<?php echo get_permalink($link_blog); ?>">Blog</a>
				</div>
				<div class="cont-cta-page">
					<a href="<?php echo get_permalink($link_meet_our_team); ?>">Meet our team</a>
				</div>
				<div class="cont-cta-page">
					<a href="<?php echo $link_test_your_spanish; ?>" target="_blank">Test your Spanish</a>
				</div>
				<div class="cont-cta-page">
					<a href="<?php echo $link_student_zone; ?>" target="_blank">Student zone</a>
				</div>
			</div>
		</div>
		<?php } ?>

	</header> <?php // .header ?>

	<div id="content" class="site-content">
