(function() {
    /* * * * * * * * * * * * * * * * Variables */


    /* * * * * * * * * * * * * * * * Functions */


    /* * * * * * * * * * * * * * * * Events  */

    jQuery(function($) {
        $(document).ready(function() {
            
        });
    });

}).call(this);

jQuery(document).ready(function() {

    // Add Border Bottom color when scroll
    jQuery(window).scroll(function() {
        var scroll = jQuery(window).scrollTop();
        if (scroll >= 10) {
            jQuery(".container-header").addClass("border-down");
        } else {
            jQuery(".container-header").removeClass("border-down");
        }
    });

    // Select course item on programs templates
    jQuery(".select-course").on('change', function(){
        jQuery('body,html').animate({ scrollTop: jQuery('#' + jQuery(this).val()).position().top });
    });

});

