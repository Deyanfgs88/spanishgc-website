<?php

/*

Template Name: Accommodations

*/

get_header(); 
the_post(); ?>

<div class="template-accommodations">
    <div class="container-fluid">
        <div class="container-accommodations">

            <div class="cta-back-title-general-accommodations">
                <div class="title-accommodations">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>
            </div>

            <div class="container-imagen-header-bg-title" style="background-image: url('<?php the_field('imagen_banner_header_accommodations'); ?>');">
                <div class="titulo">
                    <?php the_field('titulo_accommodations'); ?>
                </div>
            </div>

            <div class="container-general-info-accommodations">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="item-info">
                            <?php the_field('texto_info_accommodations'); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item-info">
                            <?php 
                                $imagenes_info = get_field('imagenes_info_accommodations'); 
                                if ($imagenes_info){
                                    echo '<div class="imagenes-info">';
                                    echo '<ul>';
                                    foreach ($imagenes_info as $imagenes) {
                                        echo '<li><img src="' . $imagenes['imagen'] . '" /></li>';
                                    }
                                    echo '</ul>';
                                    echo '</div>';
                                }
                            ?>
                        </div>
                    </div>
                </div> <?php // .row ?>
            </div> <?php // . container-general-info-accommodations ?>
            
         </div> <?php // .container-accommodations ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-accommodations ?>


<?php get_footer(); ?>