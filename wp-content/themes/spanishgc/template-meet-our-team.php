<?php

/*

Template Name: Meet our team

*/

get_header(); 
the_post(); ?>

<div class="template-meet-our-team">
    <div class="container-fluid">
        <div class="container-meet-our-team">
            <div class="cta-back-title-general-meet-our-team">
                <div class="title-meet-our-team">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <!-- <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_meet_our_team'); ?></h2> -->
                </div>
            </div>

            <div class="container-general-info-meet-our-team">

                <?php
                    $video_meet_our_team = get_field('video_meet_our_team');
                    if ($video_meet_our_team){
                ?>
                <div class="container-video">       
                    <iframe src="<?php the_field('video_meet_our_team'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div> <?php // .container-video ?>
                <?php } ?>

                <div class="texto-meet-our-team">
                    <?php the_field('texto_meet_our_team'); ?>
                </div>

            </div> <?php // . container-general-info-meet-our-team ?>
            
         </div> <?php // .container-meet-our-team ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-meet-our-team ?>




<?php get_footer(); ?>