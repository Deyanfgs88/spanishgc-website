<?php

/*

Template Name: Book Now

*/

get_header(); 
the_post(); ?>

<div class="template-book-now">
    <div class="container-fluid">
        <div class="container-book-now">
            <div class="cta-back-title-general-book-now">
                <div class="title-book-now">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_book_now'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-book-now">
                
                <?php
                    $texto_book_now = get_field('texto_principal_book_now');
                    if ($texto_book_now){
                ?>
                <div class="texto-book-now">
                    <?php the_field('texto_principal_book_now'); ?>
                </div>
                <?php } ?>

                <div class="imagen-mobile d-block d-xl-none">
                    <img src="<?php the_field('imagen_background_book_now'); ?>" alt="imagen book now">
                </div> <?php // .imagen-mobile ?>

                <div class="container-bg-formulario" style="background-image: url('<?php the_field('imagen_background_book_now'); ?>');">
                    <div class="formulario">
                        <?php echo do_shortcode( '[bkFormFoot observaciones="" imagen="" origen="14" origenForm="contacto" formulario="100"][/bkFormFoot]' ); ?>
                    </div>
                </div>

            </div> <?php // . container-general-info-book-now ?>
            
         </div> <?php // .container-book-now ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-book-now ?>




<?php get_footer(); ?>