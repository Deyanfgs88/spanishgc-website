<?php

/*

Template Name: Homepage

*/

get_header(); 
the_post(); ?>

<div class="template-homepage">
    <div class="container-fluid">

        <?php
            $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
            if ($tag_h1_meta){
        ?>
        <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
        <?php } else{ ?>
        <h1 class="d-none"><?php echo get_the_title(); ?></h1>
        <?php } ?>

        <div class="container-portada-principal">

            <div class="imagen-version-movil d-block d-md-none" style="background-image: url('<?php the_field('imagen_portada_version_movil'); ?>');"></div>

            <div class="container-video">
                <div class="video-bg-black d-none d-md-block"></div>
                <video id="home-header-video" class="d-none d-md-block" playsinline="true" autoplay="true" muted="true" loop="true">
                    <source src="<?php echo get_home_url(); ?>/wp-content/themes/spanishgc/assets/videos/homepage-spanishgc.mp4" type="video/mp4">
                </video>
            </div>

            <div class="cont-title-main-page">
                <div class="title">
                    <div class="titulo-1">
                        <?php the_field('titulo_homepage_1'); ?>
                    </div>
                    <div class="titulo-2">
                        <?php the_field('titulo_homepage_2'); ?>
                    </div>
                </div>
            </div>
            
            <?php
                $main_page_button = get_field('boton_principal_homepage');
                if ($main_page_button){
            ?>
            <div class="container-main-button">
                <a href="<?php the_field('boton_principal_homepage'); ?>"><?php the_field('texto_boton_principal_homepage'); ?></a>
            </div>
            <?php } ?>

            <div class="container-ctas-pages d-none d-md-block">
                <div class="cont-pages">
                    <div class="cont-cta-page">
                        <a href="<?php the_field('pagina_why_grancanaria') ?>">Why Gran Canaria?</a>
                    </div>
                    <div class="cont-cta-page">
                        <a href="<?php the_field('pagina_teaching_method') ?>">Teaching method</a>
                    </div>
                    <div class="cont-cta-page">
                        <a href="<?php the_field('pagina_blog') ?>">Blog</a>
                    </div>
                    <div class="cont-cta-page">
                        <a href="<?php the_field('pagina_meet_our_team') ?>">Meet our team</a>
                    </div>
                    <div class="cont-cta-page">
                        <a href="<?php the_field('url_test_your_spanish') ?>" target="_blank">Test your Spanish</a>
                    </div>
                    <div class="cont-cta-page">
                        <a href="<?php the_field('url_student_zone') ?>" target="_blank">Student zone</a>
                    </div>
                </div>
            </div>

            <!--
            <div class="container-formulario">
                <div class="text">
                    <p>I am</p>
                </div>
                <div class="formulario">
                    <form id="formulario-homepage" action="" method="post">
                        <select name="role">
                            <option value="university">university student</option>
                            <option value="child">child / teen</option>
                            <option value="professional">professional</option>
                            <option value="student50">student over 50</option>
                        </select>
                        <input id="submit-value" type="button" onclick="redirect_page();" value="Go!" >
                    </form>
                    <script type="text/javascript">
                        function redirect_page(){
                            var base_url = window.location.origin;
                            var sel = document.getElementsByName("role")[0].value;
                            if (sel == 'university'){
                                window.location = base_url + '/spanishgc/intensive-spanish-courses';
                            } else if (sel == 'child'){
                                window.location = base_url + '/spanishgc/junior-spanish-courses';
                            } else if (sel == 'professional'){
                                window.location = base_url + '/spanishgc/business-spanish-courses';
                            } else if (sel == 'student50'){
                                window.location = base_url + '/spanishgc/experience-the-canarian-life';
                            }
                        }
                    </script>
                </div>
            </div> --> <?php // .container-formulario ?>
            
        </div> <?php // .container-portada-principal ?>

        <div class="container-main-text">
            <div class="text">
                <?php the_field('texto_principal_homepage'); ?>
            </div>
        </div> <?php // .container-main-text ?>

        <div class="container-imagen-banner">
            <img src="<?php the_field('imagen_banner_homepage'); ?>" alt="banner homepage">
        </div> <?php // .container-imagen-banner ?>

        <div class="container-visit-main-site">
            <div class="text-visit">
                <?php the_field('texto_visita_pagina_principal'); ?>
            </div>
            <div class="imagen-main-site">
                <a href="<?php the_field('url_idiomasgc_web'); ?>" target="_blank">
                    <img src="<?php the_field('logo_idiomasgc_urlweb'); ?>" alt="logo idiomasgc url web">
                </a>
            </div>
        </div> <?php // .container-visit-main-site ?>

        <div class="container-partners">
            <div class="partners">

                <?php
                    $images = get_field('galeria_partners');
                    if ($images){
                        echo '<ul>';
                        foreach ($images as $image) {
                            echo '<li><a href="' . $image['url_partner'] . '" target="_blank"><img src="' . $image['imagen_partner'] . '" alt="' . $image['alt_imagen_partner'] . '" /></a></li>';
                        }
                        echo '</ul>';
                    }
                ?>

            </div>
        </div> <?php // .container-partners ?>

    </div> <?php // .container-fluid ?>
</div> <?php // .template-homepage ?>


<?php get_footer(); ?>
