<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package spanishgc
 */

get_header();
?>

<div class="template-search">
	<div class="container-fluid">
		<div class="container-search">
			
			<div class="container-general-info-search">

				<?php if ( have_posts() ) : ?>
					<div class="page-title">
						<h1>
						<?php
						/* translators: %s: search query. */
						printf( esc_html__( 'Search Results for: %s', 'spanishgc' ), '<span>' . get_search_query() . '</span>' );
						?>
						</h1>
					</div>
					<div class="cont-result-posts">
						<?php
							while ( have_posts() ) :
								the_post();
								get_template_part( 'template-parts/content', 'search' );

							endwhile;

							the_posts_navigation();
						?>
					</div>
				<?php
					else :
						get_template_part( 'template-parts/content', 'none' );
					endif;
				?>
			</div>

		</div> <?php // .container-search ?>
	</div> <?php // .container-fluid ?>
</div> <?php // .template-search ?>

<?php get_footer(); ?>
