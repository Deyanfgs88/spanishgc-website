<?php

/*

Template Name: Why Gran Canaria?

*/

get_header(); 
the_post(); ?>

<div class="template-why-gran-canaria">
    <div class="container-fluid">
        <div class="container-why-gran-canaria">

            <div class="cta-back-title-general-why-gran-canaria">
                <div class="title-why-gran-canaria">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>
            </div>

            <div class="container-imagen-header-bg-title" style="background-image: url('<?php the_field('imagen_banner_header_why_gran_canaria'); ?>');">
                <div class="titulo">
                    <?php the_field('titulo_why_gran_canaria'); ?>
                </div>
            </div>

            <div class="container-general-info-why-gran-canaria">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="item-info">
                            <?php the_field('texto_info_why_gran_canaria'); ?>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="item-info">
                            <?php 
                                $imagenes_info = get_field('imagenes_info_why_gran_canaria'); 
                                if ($imagenes_info){
                                    echo '<div class="imagenes-info">';
                                    echo '<ul>';
                                    foreach ($imagenes_info as $imagenes) {
                                        echo '<li><img src="' . $imagenes['imagen'] . '" /></li>';
                                    }
                                    echo '</ul>';
                                    echo '</div>';
                                }
                            ?>
                        </div>
                    </div>
                </div> <?php // .row ?>
            </div> <?php // . container-general-info-why-gran-canaria ?>

            <div class="footer-imagen-bg" style="background-image: url('<?php the_field('imagen_banner_footer_why_gran_canaria'); ?>');"></div>
            
         </div> <?php // .container-why-gran-canaria ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-why-gran-canaria ?>


<?php get_footer(); ?>