<?php

/*

Template Name: Work with us

*/

get_header(); 
the_post(); ?>

<div class="template-work-with-us">
    <div class="container-fluid">
        <div class="container-work-with-us">
            <div class="cta-back-title-general-work-with-us">
                <div class="title-work-with-us">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_work_with_us'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-work-with-us">
                
                <?php
                    $texto_work_with_us = get_field('texto_principal_work_with_us');
                    if ($texto_work_with_us){
                ?>
                <div class="texto-work-with-us">
                    <?php the_field('texto_principal_work_with_us'); ?>
                </div>
                <?php } ?>

                <div class="imagen-mobile d-block d-xl-none">
                    <img src="<?php the_field('imagen_background_work_with_us'); ?>" alt="imagen work with us">
                </div> <?php // .imagen-mobile ?>

                <div class="container-bg-formulario" style="background-image: url('<?php the_field('imagen_background_work_with_us'); ?>');">
                    <div class="formulario">
                        <?php echo do_shortcode( '[contact-form-7 id="278" title="Work with us"]' ); ?>
                    </div>
                </div>

            </div> <?php // . container-general-info-work-with-us ?>
            
         </div> <?php // .container-work-with-us ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-work-with-us ?>




<?php get_footer(); ?>