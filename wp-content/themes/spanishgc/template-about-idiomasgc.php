<?php

/*

Template Name: About IdiomasGC

*/

get_header(); 
the_post(); ?>

<div class="template-about-idiomasgc">
    <div class="container-fluid">
        <div class="container-about-idiomasgc">

            <div class="cta-back-title-general-about-idiomasgc">
                <div class="title-about-idiomasgc">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                </div>
            </div>

            <div class="container-general-info-about-idiomasgc">
                
                <?php
                    $video_about_idiomasgc = get_field('video_about_idiomasgc');
                    if ($video_about_idiomasgc){
                ?>
                <div class="container-video">       
                    <iframe src="<?php the_field('video_about_idiomasgc'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                </div> <?php // .container-video ?>
                <?php } ?>

                <div class="logo-lc-idiomasgc">
                    <img src="<?php the_field('logo_lc_idiomasgc_about'); ?>" alt="logo language campus idiomasgc">
                </div> <?php // .logo-lc-idiomasgc ?>

                <div class="texto-about-idiomasgc">
                    <?php the_field('texto_about_idiomasgc'); ?>
                </div>

            </div> <?php // . container-general-info-about-idiomasgc ?>
            
         </div> <?php // .container-about-idiomasgc ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-about-idiomasgc ?>


<?php get_footer(); ?>