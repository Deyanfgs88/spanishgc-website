<?php
/**
 * spanishgc Theme Customizer
 *
 * @package spanishgc
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function spanishgc_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'spanishgc_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'spanishgc_customize_partial_blogdescription',
		) );
	}

	/*----------- Customize SpanishGC --------------*/

	//------------------------------------Sections

	$wp_customize->add_section('header_section',array(
        'title'     => __('Header',THEME_LONG_NAME),
        'priority'  => 30,
	));

	$wp_customize->add_section('footer_section',array(
        'title'     => __('Footer',THEME_LONG_NAME),
        'priority'  => 30,
	));

	//------------------------------------Settings
    //------------------------------- Header
	$wp_customize->add_setting('phone')->transport = 'postMessage';
	$wp_customize->add_setting('locations_gc')->transport = 'postMessage';
	$wp_customize->add_setting('texto_locations_gc')->transport = 'postMessage';
	$wp_customize->add_setting('book_now')->transport = 'postMessage';
	$wp_customize->add_setting('texto_book_now')->transport = 'postMessage';

	$wp_customize->add_setting('why_gran_canaria')->transport = 'postMessage';
	$wp_customize->add_setting('teaching_method')->transport = 'postMessage';
	$wp_customize->add_setting('blog')->transport = 'postMessage';
	$wp_customize->add_setting('meet_our_team')->transport = 'postMessage';
	$wp_customize->add_setting('test_your_spanish')->transport = 'postMessage';
	$wp_customize->add_setting('student_zone')->transport = 'postMessage';

	//------------------------------- Footer
	$wp_customize->add_setting('facebook')->transport = 'postMessage';
	$wp_customize->add_setting('twitter')->transport = 'postMessage';
	$wp_customize->add_setting('instagram')->transport = 'postMessage';
	$wp_customize->add_setting('youtube')->transport = 'postMessage';
	$wp_customize->add_setting('linkedin')->transport = 'postMessage';
	$wp_customize->add_setting('address_tomasmorales')->transport = 'postMessage';
	$wp_customize->add_setting('email_tomasmorales')->transport = 'postMessage';
	$wp_customize->add_setting('address_mesaylopez')->transport = 'postMessage';
	$wp_customize->add_setting('email_mesaylopez')->transport = 'postMessage';
	$wp_customize->add_setting('address_sietepalmas')->transport = 'postMessage';
	$wp_customize->add_setting('email_sietepalmas')->transport = 'postMessage';
	$wp_customize->add_setting('horario_general')->transport = 'postMessage';
	$wp_customize->add_setting('trabaja_nosotros')->transport = 'postMessage';
	$wp_customize->add_setting('politica_privacidad')->transport = 'postMessage';
	$wp_customize->add_setting('politica_cookies')->transport = 'postMessage';
	$wp_customize->add_setting('politica_contratacion_cursos')->transport = 'postMessage';
	$wp_customize->add_setting('aviso_legal')->transport = 'postMessage';
	$wp_customize->add_setting('copyright')->transport = 'postMessage';


	//------------------------------------Controls
	//-------------------------------Header

	$wp_customize->add_control(
		'information_phone',
		array(
			'label'     =>__('Teléfono',THEME_LONG_NAME),
			'settings'  => 'phone',
			'priority'  => 10,
			'section'   => 'header_section'
	));

	$wp_customize->add_control(
		'information_locations_gc',
		array(
			'label'     =>__('Texto Locations in GC',THEME_LONG_NAME),
			'settings'  => 'texto_locations_gc',
			'priority'  => 10,
			'section'   => 'header_section'
	));

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'locations_control',
				array(
					'label'     =>__('Selecciona la página Locations in Gran Canaria',THEME_LONG_NAME),
					'settings'  => 'locations_gc',
					'priority'  => 10,
					'section'   => 'header_section',
			)
		)
	);

	$wp_customize->add_control(
		'information_book_now',
		array(
			'label'     =>__('Texto Book Now',THEME_LONG_NAME),
			'settings'  => 'texto_book_now',
			'priority'  => 10,
			'section'   => 'header_section'
	));

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'book_now_control',
				array(
					'label'     =>__('Selecciona la página Book now',THEME_LONG_NAME),
					'settings'  => 'book_now',
					'priority'  => 10,
					'section'   => 'header_section',
			)
		)
	);

	/** 
	 * CTAS Header
	*/

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'whygrancanaria_control',
				array(
					'label'     =>__('Selecciona la página Why Gran Canaria',THEME_LONG_NAME),
					'settings'  => 'why_gran_canaria',
					'priority'  => 10,
					'section'   => 'header_section',
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'teaching_method_control',
				array(
					'label'     =>__('Selecciona la página Teaching Method',THEME_LONG_NAME),
					'settings'  => 'teaching_method',
					'priority'  => 10,
					'section'   => 'header_section',
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'blog_control',
				array(
					'label'     =>__('Selecciona la página Blog',THEME_LONG_NAME),
					'settings'  => 'blog',
					'priority'  => 10,
					'section'   => 'header_section',
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'meet_our_team_control',
				array(
					'label'     =>__('Selecciona la página Meet our team',THEME_LONG_NAME),
					'settings'  => 'meet_our_team',
					'priority'  => 10,
					'section'   => 'header_section',
			)
		)
	);

	$wp_customize->add_control(
		'information_test_your_spanish',
		array(
			'label'     =>__('URL Test your Spanish',THEME_LONG_NAME),
			'settings'  => 'test_your_spanish',
			'priority'  => 10,
			'section'   => 'header_section'
	));

	$wp_customize->add_control(
		'information_student_zone',
		array(
			'label'     =>__('URL Studen Zone',THEME_LONG_NAME),
			'settings'  => 'student_zone',
			'priority'  => 10,
			'section'   => 'header_section'
	));

	//------------------------------- Footer

	$wp_customize->add_control(
		'information_facebook',
		array(
			'label'     =>__('URL Facebook',THEME_LONG_NAME),
			'settings'  => 'facebook',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_twitter',
		array(
			'label'     =>__('URL Twitter',THEME_LONG_NAME),
			'settings'  => 'twitter',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_instagram',
		array(
			'label'     =>__('URL Instragram',THEME_LONG_NAME),
			'settings'  => 'instagram',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_youtube',
		array(
			'label'     =>__('URL Youtube',THEME_LONG_NAME),
			'settings'  => 'youtube',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_linkedin',
		array(
			'label'     =>__('URL LinkedIn',THEME_LONG_NAME),
			'settings'  => 'linkedin',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_address_tomasmorales',
		array(
			'label'     =>__('Dirección Tomás Morales',THEME_LONG_NAME),
			'settings'  => 'address_tomasmorales',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_email_tomasmorales',
		array(
			'label'     =>__('Email Tomás Morales',THEME_LONG_NAME),
			'settings'  => 'email_tomasmorales',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_address_mesaylopez',
		array(
			'label'     =>__('Dirección Mesa y López',THEME_LONG_NAME),
			'settings'  => 'address_mesaylopez',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_email_mesaylopez',
		array(
			'label'     =>__('Email Mesa y López',THEME_LONG_NAME),
			'settings'  => 'email_mesaylopez',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_address_sietepalmas',
		array(
			'label'     =>__('Dirección Siete Palmas',THEME_LONG_NAME),
			'settings'  => 'address_sietepalmas',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_email_sietepalmas',
		array(
			'label'     =>__('Email Siete Palmas',THEME_LONG_NAME),
			'settings'  => 'email_sietepalmas',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		'information_horario_general',
		array(
			'label'     =>__('Horario General',THEME_LONG_NAME),
			'settings'  => 'horario_general',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'trabaja_nosotros_control',
				array(
					'label'     =>__('Selecciona la página Trabaja con nosotros',THEME_LONG_NAME),
					'settings'  => 'trabaja_nosotros',
					'priority'  => 10,
					'section'   => 'footer_section',
			)
		)
	);


	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'politica_privacidad_control',
				array(
					'label'     =>__('Selecciona la página Política de privacidad',THEME_LONG_NAME),
					'settings'  => 'politica_privacidad',
					'priority'  => 10,
					'section'   => 'footer_section',
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'politica_cookies_control',
				array(
					'label'     =>__('Selecciona la página Política de cookies',THEME_LONG_NAME),
					'settings'  => 'politica_cookies',
					'priority'  => 10,
					'section'   => 'footer_section',
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'politica_contratacion_cursos_control',
				array(
					'label'     =>__('Selecciona la página Política de contratacion de cursos',THEME_LONG_NAME),
					'settings'  => 'politica_contratacion_cursos',
					'priority'  => 10,
					'section'   => 'footer_section',
			)
		)
	);

	$wp_customize->add_control(
		new WP_Customize_DropdownPages_Control(
			$wp_customize,
				'aviso_legal_control',
				array(
					'label'     =>__('Selecciona la página Aviso Legal',THEME_LONG_NAME),
					'settings'  => 'aviso_legal',
					'priority'  => 10,
					'section'   => 'footer_section',
			)
		)
	);

	$wp_customize->add_control(
		'information_copyright',
		array(
			'label'     =>__('Copyright',THEME_LONG_NAME),
			'settings'  => 'copyright',
			'priority'  => 10,
			'section'   => 'footer_section'
	));

}
add_action( 'customize_register', 'spanishgc_customize_register' );


if (class_exists('WP_Customize_Control')) {
    class WP_Customize_DropdownPages_Control extends WP_Customize_Control {
         public function render_content() { ?>
        <h3><?php echo $this->label; ?></h3>
            <select  <?php $this->link(); ?>>
            <?php
                query_posts(array( 
                'post_type' => 'page',
                'post_status' => 'publish',
                'posts_per_page' => '-1',
                'orderby' => 'date', 
                'order' => 'ASC'
            ) );  
                while (have_posts()) : the_post(); ?>
                <option value = "<?php echo get_the_ID(); ?>"><?php echo get_the_title();?></option>       
                <?php endwhile ?>
            </select>
       <?php  }
    }
}

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function spanishgc_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function spanishgc_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function spanishgc_customize_preview_js() {
	wp_enqueue_script( 'spanishgc-customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'spanishgc_customize_preview_js' );
