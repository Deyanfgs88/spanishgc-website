<?php

/*

Template Name: Program Small Info

*/

get_header(); 
the_post(); ?>

<div class="template-program-small-info">
    <div class="container-fluid">
        <div class="container-program-small-info">
            <div class="cta-back-title-general-program-small-info">
                <div class="title-program-small-info">
                    <?php
                        $tag_h1_meta = get_post_meta($post->ID, "etiquetah1_62791", true);
                        if ($tag_h1_meta){
                    ?>
                    <h1 class="d-none"><?php echo get_post_meta($post->ID, "etiquetah1_62791", true); ?></h1>
                    <?php } else{ ?>
                    <h1 class="d-none"><?php echo get_the_title(); ?></h1>
                    <?php } ?>
                    <h2 class="main-title"><i class="fas fa-chevron-right"></i><?php the_field('titulo_principal_program'); ?></h2>
                </div>
            </div>

            <div class="container-general-info-program-small-info">

                <div class="imagen-text-portada-program-small-info">
                    <img src="<?php the_field('imagen_portada_program'); ?>" alt="imagen portada program">
                    <div class="text-info-program-small-info">
                        <?php the_field('texto_informacion_program'); ?>
                    </div>
                </div> <?php // .imagen-text-cta-portada-program-small-info ?>

            </div> <?php // . container-general-info-program-small-info ?>
            
         </div> <?php // .container-program-small-info ?>
    </div> <?php // .container-fluid ?>
</div><?php // .template-program-small-info ?>




<?php get_footer(); ?>